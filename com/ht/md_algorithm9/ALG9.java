/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.md_algorithm9;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class ALG9 {

    public static void main(String[] args) {
        long start = System.nanoTime();
        
        Scanner kb = new Scanner(System.in);
        String str = kb.nextLine();
        System.out.println("Input String : " + str);
        
        System.out.print("Converse String to Integer : ");
        int conv = Integer.parseInt(str);
        System.out.print(conv);
              
        System.out.println();
        
        long end = System.nanoTime();
        System.out.println("Running Time of Algorithm is " + (end - start) * 1E-9 + " secs.");
    }
    
}
